import React, {Component} from 'react';
import Button from './buttons'
import Header from "./header";
class ModalWindow extends Component {
    state={
        show:true
    }
    close=()=>{
        this.setState({show:false})
    }

    render() {
        return (
            <div >
                {this.state.show?
                    <div className={"modal-window"} paragraph={this.props.paragraph} header={this.props.header}>
                    <Header headerText={this.props.header} closePressed={this.close}/>
                    <p className={"paragraph-modal"}>{this.props.paragraph}</p>
                    <Button text={"Ok"} color={"#b3382c"}/>
                    <Button text={"Cancel"} color={"#b3382c"}/>
                    </div>
                    :null
                    }


            </div>
        );
    }
}

export default ModalWindow;