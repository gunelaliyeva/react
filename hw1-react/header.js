import React from 'react';
import './style.scss'
const Header = props => {
    return (
        <div headerText={props.headerText} onClick={props.closePressed} className={"header"}>
            <p className={"paragraph"}>{props.headerText}</p>
            <div className="close">X</div>
        </div>
    );
};

export default Header;